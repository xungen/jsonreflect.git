target: lib

lib: json.o reflect.o
	ar rcs libjson.a json.o reflect.o

test: test.o lib
	@g++ -std=c++11 -o test.out test.o libjson.a
	@echo && ./test.out
	@rm test.out

test.o: test.cpp
	g++ -std=c++11 -c -o test.o test.cpp

json.o: json.cpp
	g++ -std=c++11 -c -o json.o json.cpp

reflect.o: reflect.cpp
	g++ -std=c++11 -c -o reflect.o reflect.cpp

clean:
	@rm -rf *.o